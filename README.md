# OSA iOS #

El siguiente desafío muestra algunas de las habilidades del equipo de desarrollo iOS y ofrece la oportunidad de medir las tuyas

## Que empiece la acción ##

Problema: Se necesita una aplicación para iPhone (5s, 6 y 6plus) que muestre las películas de Star Wars que han sido estrenadas y/o anunciadas. Por cada película se muestra una imagen y el título, además si selecciono la celda se debe mostrar un pop up con la imagen de la película.

La aplicación debe verse similar a siguiente imagen:

![](https://bytebucket.org/JorgeBWG/quest-to-osa-ios/raw/7ffb14e383b3598d407d222d5db3e8492b7ca256/giphy.gif)

## Instrucciones ##
1. Haz un fork a este repositorio.
2. Escribe tu código en la rama _develop_.
3. Cuando estés seguro de tus cambios y hayas finalizado el código, haz un pull request a este repositorio.

## Debes ##

* Usar CollectionView.
* En orientación vertical deben caber 2 columnas exactas sin importar el dispositivo.
* La imagen debe estar en escala según sus proporciones, tanto en el pop up como en las celdas.
* El pop up debe aparecer y desaparecer en solo una vista.
* El pop up debe desaparecer al apretar cualquier lugar fuera del pop up.
* La interfaz debe ser responsiva al tamaño de la pantalla.

## Consejos ##

* [OMDb](http://www.omdbapi.com/) ofrece una API con las películas existentes en el mercado.
* Plantear tu propio diseño.
* Usar alguna librería.
* Usar algún administrador de librerías

Ánimo y disfruta el desafío!!!